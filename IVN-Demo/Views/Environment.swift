//
//  Environment.swift
//  IVN-iPhone

import SceneKit
import SpriteKit

protocol EnvironmentDelegate : class {
    func didMove(pos: SCNVector3,angle:SCNVector3)
    
}

class Environment: SCNView,SCNSceneRendererDelegate  {
    
    var avatar3D:[String:Avatar3D] = [String:Avatar3D]()
    
   weak var envDelegate: EnvironmentDelegate?
    
    lazy var camera: SelfAvatar = {
        let cameraNode = SelfAvatar()
        cameraNode.setCamDelegate(delegate: self)
        return cameraNode
    }()
    
    init(sncframe:CGRect,room name:String) {
        super.init(frame: sncframe,options: nil)
        //        self.showsStatistics = true
        self.translatesAutoresizingMaskIntoConstraints = false
        self.addGestureRecognizer(UIPanGestureRecognizer(target: self, action: #selector(didSwipe(_:))))
        setScene(room: name)
       
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func renderer(_ renderer: SCNSceneRenderer, didRenderScene scene: SCNScene, atTime time: TimeInterval) {
        
      //  print("scene rendered")
//        addAvatar(at: SCNVector3(x: 0, y:0.5, z: 0.5))
    }
    
    func setScene(room: String){
        let ivnScene: IVNScene = room == "office" ? Office(named: "SCNAssets.scnassets/office/office.dae")! : Classroom(named: "SCNAssets.scnassets/classroom/classroom.dae")!
        ivnScene.add(camera: self.camera)
        ivnScene.setup()
        self.delegate = self
        scene = ivnScene
        
    }
    
    
    @objc private func didSwipe(_ recognizer: UIPanGestureRecognizer){
        let translation = recognizer.location(in: self)
        camera.swipe(translation: translation, on: recognizer.state)
        recognizer.setTranslation(.zero, in: self)
    }
 
    
    func addAvatar(at position: SCNVector3 = SCNVector3(0, 0, 0),clinetId:String){
        
        if avatar3D[clinetId] == nil{
            avatar3D[clinetId] = Avatar3D(position: SCNVector3(x: camera.position.x, y:camera.position.y+0.1 , z: camera.position.z), id: clinetId)
            avatar3D[clinetId]?.name = clinetId
            scene?.rootNode.addChildNode(avatar3D[clinetId]!)
            
             print("avatar added",avatar3D)
        }
        
        
        
    }
    
    func removeAvatar(userid:String){
        if avatar3D[userid] != nil{
        avatar3D.removeValue(forKey: userid)
        scene?.rootNode.childNode(withName: userid, recursively: true)?.removeFromParentNode()
        }
        
    }
    
    func addAvatargeometry(geometry:SCNGeometry,clientId:String){
    
        if avatar3D[clientId] != nil{
              avatar3D[clientId]!.addVideo(geometry_view: geometry)
        }
      
        
    }
    
    
    
    func moveAvatar(for id: String, to vector: SCNVector3 , to angle:Float){

        print("avata id ",id)
        if avatar3D[id] != nil{
             avatar3D[id]!.move(to: vector)
        }
       
    }

 
}


extension Environment: SelfAvatarDelegate{
    func didMove(to position: SCNVector3, to angle: SCNVector3) {
        envDelegate!.didMove(pos: position, angle: angle)
    }
    

}


extension SCNVector3 {
    func distance(to position: SCNVector3) -> Float{
        let differenceVector = SCNVector3(self.x - position.x, self.y - position.y, self.z - position.z)
        return sqrtf(
            differenceVector.x * differenceVector.x +
                differenceVector.y * differenceVector.y +
                differenceVector.z * differenceVector.z
        )
    }
}
