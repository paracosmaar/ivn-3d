//
//  Message.swift

import Foundation

enum Message {
    case sdp(SessionDescription)
    case candidate(IceCandidate)
    case AvatarData(AvatarPosition)
    case Userid(UserID)
    case Disconnected(DisconnectedUser)
}

extension Message: Codable {
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        let type = try container.decode(String.self, forKey: .type)
        switch type {
        case String(describing: SessionDescription.self):
            self = .sdp(try container.decode(SessionDescription.self, forKey: .payload))
        case String(describing: IceCandidate.self):
            self = .candidate(try container.decode(IceCandidate.self, forKey: .payload))
        
        case String(describing: AvatarPosition.self):
            self = .AvatarData(try container.decode(AvatarPosition.self, forKey: .position))
            
        case String(describing: UserID.self):
            self = .Userid(try container.decode(UserID.self, forKey: .userid))
            
        case String(describing: DisconnectedUser.self):
            self = .Disconnected(try container.decode(DisconnectedUser.self, forKey: .userdisconnected))
        default:
            throw DecodeError.unknownType
        }
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        switch self {
        case .sdp(let sessionDescription):
            try container.encode(sessionDescription, forKey: .payload)
            try container.encode(String(describing: SessionDescription.self), forKey: .type)
        case .candidate(let iceCandidate):
            try container.encode(iceCandidate, forKey: .payload)
            try container.encode(String(describing: IceCandidate.self), forKey: .type)
        case .AvatarData(let avatar):
            try container.encode(avatar, forKey: .position)
              try container.encode(String(describing: AvatarPosition.self), forKey: .type)
        case .Userid(let userid):
            try container.encode(userid, forKey: .userid)
            try container.encode(String(describing: UserID.self), forKey: .type)
            
        case .Disconnected(let disconnectedUser):
            try container.encode(disconnectedUser, forKey: .userdisconnected)
            try container.encode(String(describing: DisconnectedUser.self), forKey: .type)
        }
    }
    
    enum DecodeError: Error {
        case unknownType
    }
    
    enum CodingKeys: String, CodingKey {
        case type, payload, position , userid , userdisconnected
    }
}
