//
//  GreenScreen.swift
//  IVN-iPhone


import Foundation
import AVFoundation
import UIKit
import VideoToolbox


protocol PointCoudDataDelegate:AnyObject {
    // func getpointData(img:UIImage,depth:AVDepthData)
    
    func sendPointCloudData(data:Data)
}
class VDService: AVCaptureSession {
    
    ///audio
    
    var audioBufferList = AudioBufferList()
    
    var blockBuffer : CMBlockBuffer?
    
    
    let context:CIContext = CIContext()
    let colorSpace = CGColorSpace(name: CGColorSpace.sRGB)
    var background: CIImage?
    var audiodata:Data = Data()
    var depthMap: CIImage?
    var mask: CIImage?
    weak var delegate:PointCoudDataDelegate?
    var scale: CGFloat = 0.0
    let captureQueue = DispatchQueue(label: "IVN.compression.capture")
    private let dataOutputQueue = DispatchQueue(label: "video data queue", qos: .userInitiated, attributes: [], autoreleaseFrequency: .workItem)
    var hasTrueDepthCamera = true
    private var outputSynchronizer: AVCaptureDataOutputSynchronizer?
    
    private let videoDataOutput = AVCaptureVideoDataOutput()
    private let depthDataOutput = AVCaptureDepthDataOutput()
    private let audioDataOutput = AVCaptureAudioDataOutput()
    
    override init() {
        super.init()
        
        ConfigureSession()
    }
    
    

    func stopCapture(){
        
        self.stopRunning()
    }
    
    
    func ConfigureSession(){
        
        guard let camera = AVCaptureDevice.default(.builtInTrueDepthCamera,
                                                   for: .video,
                                                   position: .front) else {
                                                    
                                                    print("No depth video camera available")
                                                    hasTrueDepthCamera = false
                                                    return
        }
        self.sessionPreset = .vga640x480
        
        do {
            let cameraInput = try AVCaptureDeviceInput(device: camera)
            self.addInput(cameraInput)
        } catch {
            fatalError(error.localizedDescription)
        }
        
        //videoOutput.setSampleBufferDelegate(self, queue: captureQueue)
        videoDataOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey as String: kCVPixelFormatType_32BGRA]
        
        self.addOutput(videoDataOutput)
        
        let videoConnection = videoDataOutput.connection(with: .video)
        videoConnection?.videoOrientation = .portrait
        videoConnection?.isVideoMirrored = true
        
        /// Capturing Depth data
        
      
       
        self.addOutput(depthDataOutput)
          depthDataOutput.isFilteringEnabled = true
        
        let depthConnection = depthDataOutput.connection(with: .depthData)
        
        depthConnection?.videoOrientation = .portrait
        depthConnection?.isVideoMirrored = true
        
        
        let outputRect = CGRect(x: 0, y: 0, width: 1, height: 1)
        let videoRect = videoDataOutput.outputRectConverted(fromMetadataOutputRect: outputRect)
        let depthRect = depthDataOutput.outputRectConverted(fromMetadataOutputRect: outputRect)
        
        scale = max(videoRect.width, videoRect.height) / max(depthRect.width, depthRect.height)
        
        
        do {
            try camera.lockForConfiguration()
            if (camera.activeDepthDataFormat?
                .videoSupportedFrameRateRanges.first?.minFrameDuration) != nil {
                camera.activeVideoMinFrameDuration = CMTime(value: 1, timescale: 10)
                camera.activeVideoMaxFrameDuration = CMTime(value: 1, timescale: 10)
            }
            
            camera.unlockForConfiguration()
        } catch {
            fatalError(error.localizedDescription)
        }
        
        outputSynchronizer = AVCaptureDataOutputSynchronizer(dataOutputs: [videoDataOutput, depthDataOutput])
        outputSynchronizer!.setDelegate(self, queue: dataOutputQueue)
        self.startRunning()
        
    }
    
    
    deinit {
        print("Videocapture deinit")
    }
    var counter = 0
}

extension VDService:AVCaptureDataOutputSynchronizerDelegate,AVCaptureAudioDataOutputSampleBufferDelegate {

    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        
        if output == audioDataOutput{
            
            print("audio data")
        }
        
    }
    
    func dataOutputSynchronizer(_ synchronizer: AVCaptureDataOutputSynchronizer, didOutput synchronizedDataCollection: AVCaptureSynchronizedDataCollection) {
        
        
        
        guard let syncedDepthData: AVCaptureSynchronizedDepthData =
            synchronizedDataCollection.synchronizedData(for: depthDataOutput) as? AVCaptureSynchronizedDepthData,
            let syncedVideoData: AVCaptureSynchronizedSampleBufferData =
            synchronizedDataCollection.synchronizedData(for: videoDataOutput) as? AVCaptureSynchronizedSampleBufferData
            //            let audioData = synchronizedDataCollection.synchronizedData(for: audioDataOutput) as? AVCaptureSynchronizedSampleBufferData
            else {
                //   print("Error getting Audio Buffer")
                return
        }
        
        
        
        
        if syncedDepthData.depthDataWasDropped || syncedVideoData.sampleBufferWasDropped {
            return
        }
        
        let depthData = syncedDepthData.depthData
        
        let sampleBuffer = syncedVideoData.sampleBuffer
        let imageBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer)!
        let ciimage : CIImage = CIImage(cvPixelBuffer: imageBuffer)
        guard let imageconverted = self.convert(cmage: ciimage) else{return}
        
        guard let image = tj_UIImageHEICRepresentation(imageconverted, 0.2) else {
            return
        }
        
        guard let cimage = CIImage(data: image) else {
            return
        }
       
        if let imgdepthdata = saveWithDepth(image: cimage, depthdata: depthData) {
          
            delegate?.sendPointCloudData(data: imgdepthdata)
        }
    }
    
    func convert(cmage:CIImage) -> UIImage?
    {
         let context:CIContext = CIContext.init(options: nil)
         let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
         let image:UIImage = UIImage.init(cgImage: cgImage)
         return image
    }
    
    func saveWithDepth(image: CIImage,depthdata:AVDepthData) -> Data? {
    
        let imageData = context.heifRepresentation(of: image, format: .BGRA8, colorSpace: colorSpace!,options: [CIImageRepresentationOption.avDepthData: depthdata])
        
        return imageData
    }
    
}



