//
//  PeerConnections.swift
//  IVN-Demo
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation
import WebRTC

class UserPeerConnection:RTCPeerConnection{
    
    
    var config:RTCConfiguration
    var constraints:RTCMediaConstraints
    
    init(config:RTCConfiguration,constraints:RTCMediaConstraints) {
        
        self.config = config
        self.constraints = constraints
        
    }
    
}
