//
//  PointCloud.swift


import SceneKit
import AVFoundation

struct PointCloudVertex {
    var x: Float, y: Float, z: Float
    var r: Float, g: Float, b: Float
}

@objc class PointCloud: NSObject {
     private let zCamera: Float = 0.25
    var pointCloud : [SCNVector3] = []
    var colors: [UInt8] = []
    
     func pointCloudNode() -> SCNGeometry {
        let points = self.pointCloud
        var vertices = Array(repeating: PointCloudVertex(x: 0,y: 0,z: 0,r: 0,g: 0,b: 0), count: points.count)
        for i in 0...(points.count-1) {
            
            let p = points[i]
            if Float(p.z) > 0.00020{
                vertices[i].x = Float(p.x)
                vertices[i].y = Float(p.y)
                vertices[i].z = Float(p.z)
                //                          print("z data",vertices[i].z)
                vertices[i].r = 0.5 * Float(colors[i * 4]) / 255.0
                vertices[i].g = 0.5 * Float(colors[i * 4 + 1]) / 255.0
                vertices[i].b = 0.5 * Float(colors[i * 4 + 2]) / 255.0
            }
            
            
        }
        
        let node = buildNode(points: vertices)
        //        node.name = "pointcloud"
        return node
    }
    
    private func buildNode(points: [PointCloudVertex]) -> SCNGeometry {
        let vertexData = NSData(
            bytes: points,
            length: MemoryLayout<PointCloudVertex>.size * points.count
        )
        let positionSource = SCNGeometrySource(
            data: vertexData as Data,
            semantic: SCNGeometrySource.Semantic.vertex,
            vectorCount: points.count,
            usesFloatComponents: true,
            componentsPerVector: 3,
            bytesPerComponent: MemoryLayout<Float>.size,
            dataOffset: 0,
            dataStride: MemoryLayout<PointCloudVertex>.size
        )
        let colorSource = SCNGeometrySource(
            data: vertexData as Data,
            semantic: SCNGeometrySource.Semantic.color,
            vectorCount: points.count,
            usesFloatComponents: true,
            componentsPerVector: 3,
            bytesPerComponent: MemoryLayout<Float>.size,
            dataOffset: MemoryLayout<Float>.size * 3,
            dataStride: MemoryLayout<PointCloudVertex>.size
        )
        let element = SCNGeometryElement(
            data: nil,
            primitiveType: .point,
            primitiveCount: points.count,
            bytesPerIndex: MemoryLayout<Int>.size
        )
        
        // for bigger dots
        element.pointSize = 1
        element.minimumPointScreenSpaceRadius = 1
        element.maximumPointScreenSpaceRadius = 5
        
        let pointsGeometry = SCNGeometry(sources: [positionSource, colorSource], elements: [element])
        
        return pointsGeometry
    }
    
    
      func drawPointCloud(image:UIImage?,depthData:AVDepthData?) -> SCNGeometry {
        
           guard let colorImage = image, let cgColorImage = colorImage.cgImage else { fatalError() }
           guard let depthData = depthData else { fatalError() }
           
           let depthPixelBuffer = depthData.depthDataMap
           let width  = CVPixelBufferGetWidth(depthPixelBuffer)
           let height = CVPixelBufferGetHeight(depthPixelBuffer)
           
           let resizeScale = CGFloat(width) / colorImage.size.width
           let resizedColorImage = CIImage(cgImage: cgColorImage).transformed(by: CGAffineTransform(scaleX: resizeScale, y: resizeScale))
           guard let pixelDataColor = resizedColorImage.createCGImage().pixelData() else { fatalError() }
          
           // Applying Histogram Equalization
           //        let depthImage = CIImage(cvPixelBuffer: depthPixelBuffer).applyingFilter("YUCIHistogramEqualization")
           //        let context = CIContext(options: nil)
           //        context.render(depthImage, to: depthPixelBuffer, bounds: depthImage.extent, colorSpace: nil)
           
           let pixelDataDepth: [Float32]
           pixelDataDepth = depthPixelBuffer.grayPixelData()
           
           
           let zMax = pixelDataDepth.max()!
           let zNear = zCamera - 0.2
           let zScale = zMax > zNear ? zNear / zMax : 1.0
           // print("z scale: \(zScale)")
           let xyScale: Float = 0.0002
            
           let pointCloud: [SCNVector3] = pixelDataDepth.enumerated().map {
               let index = $0.offset
               // Adjusting scale and translating to the center
               let x = Float(index % width - width / 2) * xyScale
               let y = Float(height / 2 - index / width) * xyScale
               // z comes as Float32 value
               let z = Float($0.element) * zScale
            
               return SCNVector3(x, y, z)
           }
        
        
         
         self.pointCloud = pointCloud
          self.colors = pixelDataColor
        
           let geometry = pointCloudNode()
           
           return geometry
       }
}


extension CGImage {
    
    func pixelData() -> [UInt8]? {
        guard let colorSpace = colorSpace else { return nil }
        
        let totalBytes = height * bytesPerRow
        var pixelData = [UInt8](repeating: 0, count: totalBytes)
        
        guard let context = CGContext(
            data: &pixelData,
            width: width,
            height: height,
            bitsPerComponent: bitsPerComponent,
            bytesPerRow: bytesPerRow,
            space: colorSpace,
            bitmapInfo: bitmapInfo.rawValue)
            else { fatalError() }
        context.draw(self, in: CGRect(x: 0.0, y: 0.0, width: CGFloat(width), height: CGFloat(height)))
        
        return pixelData
    }
}

extension CVPixelBuffer {
    
    func grayPixelData() -> [Float32] {
        CVPixelBufferLockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))
        let width = CVPixelBufferGetWidth(self)
        let height = CVPixelBufferGetHeight(self)
        var pixelData = [Float32](repeating: 0, count: Int(width * height))
        for yMap in 0 ..< height {
            let rowData = CVPixelBufferGetBaseAddress(self)! + yMap * CVPixelBufferGetBytesPerRow(self)
            let data = UnsafeMutableBufferPointer<Float>(start: rowData.assumingMemoryBound(to: Float.self), count: width)
            for index in 0 ..< width {
                pixelData[index +  width * yMap] = Float32(data[index / 2])
            }
        }
        CVPixelBufferUnlockBaseAddress(self, CVPixelBufferLockFlags(rawValue: 0))
        return pixelData
    }
}



