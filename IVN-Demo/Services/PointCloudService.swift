//
//  PointCloudService.swift
//  IVNSample
//
//  Created by sanam on 5/25/20.
//  Copyright © 2020 KTMStudio. All rights reserved.
//

import AVFoundation
import UIKit

class PoinCloudService:NSObject {

    var greenScreen:VDService?
    var datareceived = true
    //var sockethandler = SocketHandler()
  
    private var image: UIImage?
    
    lazy var preview:AVCaptureVideoPreviewLayer = {
        
        let pre = AVCaptureVideoPreviewLayer()
        pre.videoGravity = .resizeAspectFill
        pre.frame = CGRect(x: 10, y: 50, width: 100, height: 130)
        return pre
        
    }()
    
    
    
    override init() {
        super.init()
        
        setupVD()
        
    }
    
 
    private func setupVD(){
        greenScreen = VDService()
        if greenScreen!.hasTrueDepthCamera{
            greenScreen?.delegate = self
            preview.session = greenScreen
        }else{
            
            
        }
    }
    

    
    
    
}


extension PoinCloudService:PointCoudDataDelegate{
    
    func sendPointCloudData(data: Data) {
        
        if data.count < 50000{
           
            _ = try! (data as NSData).compressed(using: .lzfse)
              //  self.sockethandler.sendPointCloudData(data: Data(referencing: compressedData))
            
        }
    }
}
