//
//  PeerConnectionHandler.swift
//  IVN-Demo
//
//  Created by sanam on 7/13/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation
import WebRTC

class PeerConnectionHandler:NSObject{
    
    
    static let instance = PeerConnectionHandler()
    
    
    private override init() {
        
    }
    
    static let factory: RTCPeerConnectionFactory = {
           RTCInitializeSSL()
           let videoEncoderFactory = RTCDefaultVideoEncoderFactory()
           let videoDecoderFactory = RTCDefaultVideoDecoderFactory()
           return RTCPeerConnectionFactory(encoderFactory: videoEncoderFactory, decoderFactory: videoDecoderFactory)
       }()
    
     var peerconnectionList:[String:RTCPeerConnection] = [String:RTCPeerConnection]()
    
     let config = RTCConfiguration()
    let constraints = RTCMediaConstraints(mandatoryConstraints: nil,
                                                 optionalConstraints: ["DtlsSrtpKeyAgreement":kRTCMediaConstraintsValueTrue])
           
    
    
    
    func AddConfiguration(iceServers: [String]){
        config.iceServers = [RTCIceServer(urlStrings: ["turn:34.214.76.102:3478?transport=udp"], username: "ivnuser", credential: "ivnpassword"),RTCIceServer(urlStrings: iceServers)]
              
        config.sdpSemantics = .unifiedPlan
    }
    
    func createPeerConnection(client:String){
        
        if client != myuserid && self.getPeerConnection()[client] == nil{
            let peer = PeerConnectionHandler.factory.peerConnection(with: self.config, constraints: self.constraints, delegate: nil)
            
            self.peerconnectionList[client] = peer
            
        }
           
    }
    
    
    func getPeerConnection() -> [String:RTCPeerConnection]{
      
        return peerconnectionList
    }
    
    func removeUser(userid:String){
        
        if self.peerconnectionList[userid] != nil{
        self.peerconnectionList.removeValue(forKey: userid)
        }
        
    }
    
    
}

