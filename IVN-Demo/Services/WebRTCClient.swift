//
//  WebRTCClient.swift


import Foundation
import WebRTC

protocol WebRTCClientDelegate: class {
    
    func webRTCClient(_ client: WebRTCClient, didDiscoverLocalCandidate candidate: RTCIceCandidate)
    func webRTCClient(_ client: WebRTCClient, didChangeConnectionState state: RTCIceConnectionState)
    func webRTCClient(_ client: WebRTCClient, didReceiveData data: Data, clientId:String)
    func webRTCClient(_ client: WebRTCClient, datachannelready: Bool)
    func webRTCClient(_ client: WebRTCClient, didCompleteIceGathering: Bool)
    
}

 final class WebRTCClient: NSObject {
    
    weak var delegate: WebRTCClientDelegate?
    private let rtcAudioSession =  RTCAudioSession.sharedInstance()
    private let audioQueue = DispatchQueue(label: "audio")
    private let mediaConstrains = [kRTCMediaConstraintsOfferToReceiveAudio: kRTCMediaConstraintsValueTrue]
    private var localDataChannel: [RTCDataChannel] = [RTCDataChannel]()
    private var remoteDataChannel: [RTCDataChannel] = [RTCDataChannel]()
    
    private var audioTrack:RTCAudioTrack?
    let peerconnnectionHandler = PeerConnectionHandler.instance
    
    
    
    
    func addPeerConnection(userpeerconnection:String){
        
        self.createMediaSenders(peerconnection:userpeerconnection)
        self.configureAudioSession()
        peerconnnectionHandler.getPeerConnection()[userpeerconnection]!.delegate = self
            
        
    }
    
    func stoppeerconnection(peerconnection:String){
        
        print("peer connection stopped")
        
        if self.peerconnnectionHandler.getPeerConnection()[peerconnection] != nil{
             self.peerconnnectionHandler.getPeerConnection()[peerconnection]!.close()
        }
       
           
        
    
        
        
    }
    
    // MARK: Signaling
    func offer(peerconnection:String,completion: @escaping (_ sdp: RTCSessionDescription) -> Void) {
        let constrains = RTCMediaConstraints(mandatoryConstraints: self.mediaConstrains,
                                             optionalConstraints: nil)
      
            
             peerconnnectionHandler.getPeerConnection()[peerconnection]!.offer(for: constrains) { (sdp, error) in
                guard let sdp = sdp else {
                    print("no sdp ")
                    return
                }
                
                self.peerconnnectionHandler.getPeerConnection()[peerconnection]!.setLocalDescription(sdp, completionHandler: { (error) in
                      print("sdp value ",sdp)
                    completion(sdp)
                })
            }

    }
    
    func answer(peerconnection:String,completion: @escaping (_ sdp: RTCSessionDescription) -> Void)  {
        let constrains = RTCMediaConstraints(mandatoryConstraints: self.mediaConstrains,
                                             optionalConstraints: nil)
        
             peerconnnectionHandler.getPeerConnection()[peerconnection]!.answer(for: constrains) { (sdp, error) in
                guard let sdp = sdp else {
                    return
                }
                
                self.peerconnnectionHandler.getPeerConnection()[peerconnection]!.setLocalDescription(sdp, completionHandler: { (error) in
                    completion(sdp)
                })
            }
            
    }
    
    func set(peerconnection:String,remoteSdp: RTCSessionDescription, completion: @escaping (Error?) -> ()) {
        
        peerconnnectionHandler.getPeerConnection()[peerconnection]!.setRemoteDescription(remoteSdp, completionHandler: completion)
        
    }
    
    func set(peerconnection:String,remoteCandidate: RTCIceCandidate) {
       
        peerconnnectionHandler.getPeerConnection()[peerconnection]!.add(remoteCandidate)
        
    }

    
    private func configureAudioSession() {
        self.rtcAudioSession.lockForConfiguration()
        do {
            try self.rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
            try self.rtcAudioSession.setMode(AVAudioSession.Mode.voiceChat.rawValue)
        } catch let error {
            debugPrint("Error changeing AVAudioSession category: \(error)")
        }
        self.rtcAudioSession.unlockForConfiguration()
    }
    
    private func createMediaSenders(peerconnection:String) {
        
        let streamId = "stream"
       
        // Audio
        self.audioTrack = self.createAudioTrack()
        
            
         peerconnnectionHandler.getPeerConnection()[peerconnection]!.add(audioTrack!, streamIds: [streamId])
        if let dataChannel = createDataChannel(peerconnection: peerconnection) {
                    dataChannel.delegate = self
              self.localDataChannel.append(dataChannel)
                }
    }
    
    
    private func createAudioTrack() -> RTCAudioTrack {
        let audioConstrains = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: nil)
        let audioSource = PeerConnectionHandler.factory.audioSource(with: audioConstrains)
        let audioTrack = PeerConnectionHandler.factory.audioTrack(with: audioSource, trackId: "audio0")
        return audioTrack
    }
    
    // MARK: Data Channels
    private func createDataChannel(peerconnection:String) -> RTCDataChannel? {
        let config = RTCDataChannelConfiguration()
        config.isOrdered = false
//        config.maxRetransmits = 0
        config.maxPacketLifeTime = 0
        
        guard let dataChannel =  peerconnnectionHandler.getPeerConnection()[peerconnection]!.dataChannel(forLabel: peerconnection, configuration: config) else {
            debugPrint("Warning: Couldn't create data channel.")
            return nil
        }
        return dataChannel
    }
    
    func sendData(_ data: Data) {
        
        let buffer = RTCDataBuffer(data: data, isBinary: true)
        for datachannel in self.remoteDataChannel{
                      datachannel.sendData(buffer)
               }
    }
}

extension WebRTCClient: RTCPeerConnectionDelegate {
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        debugPrint("peerConnection new signaling state: \(stateChanged)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        debugPrint("peerConnection did add stream")
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        debugPrint("peerConnection did remote stream")
    }
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        debugPrint("peerConnection should negotiate")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        debugPrint("peerConnection new connection state: \(newState)")
        self.delegate?.webRTCClient(self, didChangeConnectionState: newState)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        switch newState {
        case .gathering:
            self.delegate?.webRTCClient(self, didCompleteIceGathering:false)
        case .complete:
            self.delegate?.webRTCClient(self, didCompleteIceGathering: true)
        case .new:
            self.delegate?.webRTCClient(self, didCompleteIceGathering: false)
        @unknown default:
            print("unknown icegathering state")
        }
        
        debugPrint("peerConnection new gathering state: \(newState)")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
    
        self.delegate?.webRTCClient(self, didDiscoverLocalCandidate: candidate)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        debugPrint("peerConnection did remove candidate(s)")
    }
    
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        debugPrint("peerConnection did open data channel")
        self.delegate?.webRTCClient(self, datachannelready: true)
      self.remoteDataChannel.append(dataChannel)
    }
    
}

// MARK:- Audio control
extension WebRTCClient {
    func muteAudio(peerconnection:String) {
        self.setAudioEnabled(peerconnection:peerconnection, false)
    }
    
    func unmuteAudio(peerconnection:String) {
        self.setAudioEnabled(peerconnection:peerconnection, true)
    }
    
    // Fallback to the default playing device: headphones/bluetooth/ear speaker
    func speakerOff() {
        self.audioQueue.async { [weak self] in
            guard let self = self else {
                return
            }
            
            self.rtcAudioSession.lockForConfiguration()
            do {
                try self.rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
                try self.rtcAudioSession.overrideOutputAudioPort(.none)
            } catch let error {
                debugPrint("Error setting AVAudioSession category: \(error)")
            }
            self.rtcAudioSession.unlockForConfiguration()
        }
    }
    
    // Force speaker
    func speakerOn() {
        self.audioQueue.async { [weak self] in
            guard let self = self else {
                return
            }
            
            self.rtcAudioSession.lockForConfiguration()
            do {
                try self.rtcAudioSession.setCategory(AVAudioSession.Category.playAndRecord.rawValue)
                try self.rtcAudioSession.overrideOutputAudioPort(.speaker)
                try self.rtcAudioSession.setActive(true)
            } catch let error {
                debugPrint("Couldn't force audio to speaker: \(error)")
            }
            self.rtcAudioSession.unlockForConfiguration()
        }
    }
    
    private func setAudioEnabled(peerconnection:String,_ isEnabled: Bool) {
       
        let audioTracks =  peerconnnectionHandler.getPeerConnection()[peerconnection]!.transceivers.compactMap { return $0.sender.track as? RTCAudioTrack }
        audioTracks.forEach { $0.isEnabled = isEnabled
            
        }
    }
}

extension WebRTCClient: RTCDataChannelDelegate {
    func dataChannelDidChangeState(_ dataChannel: RTCDataChannel) {
        debugPrint("dataChannel did change state: \(dataChannel.readyState)")
    }
    
    func dataChannel(_ dataChannel: RTCDataChannel, didReceiveMessageWith buffer: RTCDataBuffer) {
        
//        print("datachannel id ",dataChannel.label)
        self.delegate?.webRTCClient(self, didReceiveData: buffer.data, clientId: dataChannel.label)
    }
}
