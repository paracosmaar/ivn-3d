//
//  SignalClient.swift

import Foundation
import WebRTC
import SceneKit

protocol SignalClientDelegate: class {
    func signalClientDidConnect(_ signalClient: SignalingClient)
    func signalClientDidDisconnect(_ signalClient: SignalingClient)
    func signalClient(_ signalClient: SignalingClient, didReceiveRemoteSdp sdp: RTCSessionDescription,client id:String, client type:SdpType)
    func signalClient(_ signalClient: SignalingClient, didReceiveCandidate candidate: RTCIceCandidate,client id:String)
    func signalClient(_ signalClient: SignalingClient, didReceiveAvatarData avatar: AvatarPosition)
    func signalClient(_ signalClient: SignalingClient, didReceiveUserID userid: UserID)
    func signalClient(_ signalClient: SignalingClient, didUserDisconnected user: DisconnectedUser)
}

final class SignalingClient {
    
    private let decoder = JSONDecoder()
    private let encoder = JSONEncoder()
     var webSocket: WebSocketProvider
    weak var delegate: SignalClientDelegate?
    
    init(webSocket: WebSocketProvider) {
        self.webSocket = webSocket
    }
    
    func connect() {
        self.webSocket.delegate = self
        self.webSocket.connect()
    }
    
    func disconnect(){
        
        self.webSocket.disconnect()
    }
    
    deinit {
//        self.webSocket.disconnect()
        print("signalling deinit")
    
        
    }
    
    func send(sdp rtcSdp: RTCSessionDescription, userid:String,clientid:String) {
        let message = Message.sdp(SessionDescription(from: rtcSdp, userid: userid,clientid: clientid))
        do {
            let dataMessage = try self.encoder.encode(message)
            
            self.webSocket.send(data: dataMessage)
        }
        catch {
            debugPrint("Warning: Could not encode sdp: \(error)")
        }
    }
    
    func send(candidate rtcIceCandidate: RTCIceCandidate,userid:String,clientID:String) {
        let message = Message.candidate(IceCandidate(from: rtcIceCandidate,userid: userid,clientId: clientID))
        do {
            let dataMessage = try self.encoder.encode(message)
            self.webSocket.send(data: dataMessage)
        }
        catch {
            debugPrint("Warning: Could not encode candidate: \(error)")
        }
    }
    
    
    func send(room:String,avatar position:SCNVector3 ,angle:Float){
        let message = Message.AvatarData(AvatarPosition(room: room, userid:myuserid, x: position.x, y: position.y, z: position.z,rotation: angle))
              do {
                  let dataMessage = try self.encoder.encode(message)
                  
                  self.webSocket.send(data: dataMessage)
              }
              catch {
                  debugPrint("Warning: Could not encode sdp: \(error)")
              }
        
    }
    
    func sendUserID(userid:String,room:String){
        
        let message = Message.Userid(UserID(userid: userid, roomName: room, connectedUsers: [userid], connectedUserRooms: [userid:room]))
        do {
            let userdata = try self.encoder.encode(message)
            self.webSocket.send(data: userdata)
        } catch
        {
              debugPrint("Warning: Could not encode sdp: \(error)")
        }
    }
}


extension SignalingClient: WebSocketProviderDelegate {
    func webSocketDidConnect(_ webSocket: WebSocketProvider) {
        self.delegate?.signalClientDidConnect(self)
    }
    
    func webSocketDidDisconnect(_ webSocket: WebSocketProvider) {
        self.delegate?.signalClientDidDisconnect(self)
        
        // try to reconnect every two seconds
//        DispatchQueue.global().asyncAfter(deadline: .now() + 5) {
//            debugPrint("Trying to reconnect to signaling server...")
//            self.webSocket.connect()
//        }
    }
    
    
    
    func webSocket(_ webSocket: WebSocketProvider, didReceiveData data: Data) {
        
        
        
        
        
        
        let message: Message
        do {
            message = try self.decoder.decode(Message.self, from: data)
            
//            print("socket msg ",message)
        }
        catch {
            debugPrint("Warning: Could not decode incoming message: \(error)")
            return
        }
        
        switch message {
        case .candidate(let iceCandidate):
            
         
            self.delegate?.signalClient(self, didReceiveCandidate: iceCandidate.rtcIceCandidate,client:iceCandidate.userid)
        case .sdp(let sessionDescription):
             
              self.delegate?.signalClient(self, didReceiveRemoteSdp: sessionDescription.rtcSessionDescription, client: sessionDescription.userid!,client:sessionDescription.type)
        case .AvatarData(let avatardata):
            self.delegate?.signalClient(self, didReceiveAvatarData: avatardata)
            
        case .Userid(let userdata):
            self.delegate?.signalClient(self, didReceiveUserID: userdata)
        case .Disconnected(let disconnecteduser):
            self.delegate?.signalClient(self, didUserDisconnected: disconnecteduser)
            
        }

    }
}
