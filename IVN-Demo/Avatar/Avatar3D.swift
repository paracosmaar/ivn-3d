//
//  Avatar3D.swift
//  IVN-iPhone
//
//  Created by VRLab on 2/20/20.
//  Copyright © 2020 Paracosma. All rights reserved.
//

import Foundation
import SceneKit


class Avatar3D:SCNNode{
    
    var avatarId:String
    
    
    init(position: SCNVector3 = SCNVector3(0, 0, 0),id:String) {
         self.avatarId = id
        super.init()
       
        self.position = position
//        pivot = SCNMatrix4MakeTranslation(0, -2, 0)
        self.scale = SCNVector3(12, 12, 12)
        setupYFreeConstraint()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
      func setupYFreeConstraint(){
          let yFreeConstraint = SCNBillboardConstraint()
          yFreeConstraint.freeAxes = .Y
          self.constraints = [yFreeConstraint]
      }
    
      func move(to vector: SCNVector3){
          self.runAction(SCNAction.move(to: vector, duration: 0.4))
      }
    
    func roatate(to angle:Float){
        
        self.runAction(SCNAction.rotateTo(x: 0, y: CGFloat(angle), z: 0, duration: 0.3))
    }
      
      func addVideo(geometry_view: SCNGeometry){
        DispatchQueue.main.async {
                      
            self.geometry = geometry_view
           // self.addbox()
        }
        
      }
      
      
      func remove(){
          geometry = nil
          removeFromParentNode()
      }
}
