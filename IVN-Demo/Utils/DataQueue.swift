//
//  DataQueue.swift

//  Created by sanam on 5/31/20.


import Foundation


class Queue<T> {
   var elements: [T] = []

  func enqueue(_ value: T) {
    elements.append(value)
  }

  func dequeue() -> T? {
    guard !elements.isEmpty else {
      return nil
    }
    return elements.removeFirst()
  }

  var head: T? {
    return elements.first
  }

  var tail: T? {
    return elements.last
  }
}


class DataQueue {
   var elements:Data = Data()

  func enqueue(_ value: Data) {
    elements.append(value)
  }

  func dequeue() -> Data? {
    guard !elements.isEmpty else {
      return nil
    }
    return elements
  }


}
