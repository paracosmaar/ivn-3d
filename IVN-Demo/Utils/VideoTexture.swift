//
//  VideoTexture.swift


import Foundation
import SpriteKit
import AVFoundation


class VideoTexture: SKScene {
    
    init(filename: String, size: CGSize) {
        super.init(size: size)
        play(videoName: filename)
    }
    
    private func play(videoName: String){
        
        let videoURL = Bundle.main.url(forResource: videoName, withExtension: "mp4")!
        
        print("vidoe url ",videoURL)
        let player = AVPlayer(url: videoURL)
        scaleMode = .aspectFit
        
        let videoSpriteNode = SKVideoNode(avPlayer: player)
        videoSpriteNode.position = CGPoint(x: size.width/2, y: size.height/2)
        videoSpriteNode.size = size
        videoSpriteNode.yScale = -1
        videoSpriteNode.play()
        addChild(videoSpriteNode)
        
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime,object: player.currentItem,queue: .main){[unowned player] _ in
                player.seek(to: CMTime.zero)
                player.play()
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
}
