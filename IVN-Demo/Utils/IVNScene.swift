//
//  IVNScene.swift
//  IVN-iPhone
//


import Foundation
import SceneKit
import SpriteKit

class IVNScene: SCNScene{
    func setup(){
        addSkySphere()
        addLight()
    }
    
    func addSkySphere(){
        background.contents = UIImage(named: "sky_sphere")
    }
    
    func addLight(){
        let ambientLightNode = SCNNode()
        ambientLightNode.light = SCNLight()
        ambientLightNode.light!.type = .ambient
        ambientLightNode.light!.color = UIColor.white
        rootNode.addChildNode(ambientLightNode)
    }
    
    func add(camera: SelfAvatar){
        rootNode.addChildNode(camera)
    }
}


class Classroom: IVNScene{
    override func add(camera: SelfAvatar) {
        super.add(camera: camera)
        
        let x = Double.random(in: -3.0...3.0)
        let z = Double.random(in: 0...10.0)
        
        camera.position = SCNVector3(x,2,z)
        camera.eulerAngles.y = 0
    }
}


class Office: IVNScene{
    override func add(camera: SelfAvatar){
        super.add(camera: camera)
        
        let positions = [
            (x: -17.6, y: 2.0, z: 4, a: -Float.pi/2),
            (x: -14.6, y: 2, z: 4, a: Float.pi/2),
            (x: -16, y: 2, z: 0.2, a: Float.pi),
            (x: -16.1, y: 2, z: 7.7, a: 0)
        ]
        
        let rnd = Int.random(in: 0...3)
        
        let pos = positions[rnd]
        camera.position = SCNVector3(pos.x, pos.y, pos.z)
        print("debug  index",rnd)
        camera.eulerAngles.y = pos.a
    }
    
    override func setup() {
        super.setup()
        addVideoForTv(named: "tv01")
        addVideoForTv(named: "tv03")
    }
    
    func addVideoForTv(named tvName: String){
        let tv = rootNode.childNode(withName: tvName, recursively: true)!
        let plane = SCNNode(geometry:SCNPlane(width: 2.225, height: 1.22))
        tv.addChildNode(plane)
        plane.position = SCNVector3(-38.998, 13.37, 1.935)
        plane.eulerAngles = SCNVector3(x: .pi/2, y: 0, z: 0)
        let skScene = VideoTexture(filename: "test1", size: CGSize(width: 640, height: 480))
        plane.geometry?.firstMaterial?.diffuse.contents = skScene
    }
}
