//
//  FlyCamera.swift
//  IVN-iPhone


import SceneKit


class FlyCamera: SCNNode{
    private var swipeBeginOn: (x: CGFloat,y: CGFloat) = (0,0)
    private var translationOnSwipe: (x: CGFloat,y: CGFloat) = (0,0)
    private var canRotate = false{
        didSet{
            if canRotate{
                self.rotate()
            }
        }
    }
    private var canMove = false{
        didSet{
            if canMove{
                self.move()
            }
        }
    }
    
    override init() {
        super.init()
        self.camera = SCNCamera()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func swipe(translation: CGPoint ,on state: UIGestureRecognizer.State){
        let threshold: CGFloat = 30
        translationOnSwipe = (x: translation.x - swipeBeginOn.x, y: translation.y - swipeBeginOn.y)
        
        switch state {
        case .changed:
            //threshold helps ignore the unwanted swipe for better ux
            if (translationOnSwipe.y > threshold || translationOnSwipe.y < -threshold){
                if !canMove{
                    canMove = true
                }
            }else{
                if canMove{
                    canMove = false
                }
            }
            
            if translationOnSwipe.x > threshold || translationOnSwipe.x < -threshold{
                if !canRotate{
                    canRotate = true
                }
            }
            else{
                if canRotate{
                    canRotate = false
                }
            }
        case .began:
            swipeBeginOn = (translation.x,translation.y)
        case .ended:
            swipeBeginOn = (0,0)
            translationOnSwipe = (0,0)
            canRotate = false
            canMove = false
        default:
            break
        }
    }
    
    //values 2500 and 10000 are hit and trial
    fileprivate func move(){
        // third row of the node's worldTransform matrix corresponds to it's z-forward axis
        let toAddPosition = SCNVector3(self.worldTransform.m31 * Float(translationOnSwipe.y/2500), self.worldTransform.m32 * Float(translationOnSwipe.y/2500), self.worldTransform.m33 * Float(translationOnSwipe.y/2500))
        self.position = SCNVector3(self.position.x + toAddPosition.x, self.position.y + toAddPosition.y, self.position.z + toAddPosition.z)
        notifyMovementToDelegate()
        DispatchQueue.main.asyncAfter(deadline: .now()+1/60) {
            
            if self.canMove{
                self.move()
            }
        }
    }
    
    private func rotate(){
        self.eulerAngles.y -= Float(translationOnSwipe.x/10000)
        DispatchQueue.main.asyncAfter(deadline: .now()+1/60) {
            if self.canRotate{
                self.rotate()
                self.notifyMovementToDelegate()
            }
        }
    }
    
    fileprivate func notifyMovementToDelegate(){
    }
    
 
    
}



// removing responsibility of sending position from flycamera
// Attempt to use camera as user's self; like hero in FPS

protocol SelfAvatarDelegate:class {
    
    func didMove(to position: SCNVector3 , to angle:SCNVector3)
    
}


class SelfAvatar: FlyCamera{
   weak private var camDelegate: SelfAvatarDelegate!
    
    func setCamDelegate(delegate: SelfAvatarDelegate){
        camDelegate = delegate
    }
    
    override func notifyMovementToDelegate() {
        camDelegate.didMove(to: self.position, to: self.eulerAngles)
    }
 
}
