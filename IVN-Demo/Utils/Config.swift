//
//  Config.swift

//

import Foundation
let myuserid = UIDevice.current.identifierForVendor!.uuidString
// Set this to the machine's address which runs the signaling server
//fileprivate let defaultSignalingServerUrl = URL(string: "ws://ec2-34-214-76-102.us-west-2.compute.amazonaws.com:59000")!
fileprivate let defaultSignalingServerUrl = URL(string: "ws://192.168.1.72:9090")!

fileprivate let defaultIceServers = ["stun:34.214.76.102:3478","stun:stun.l.google.com:19302",
                                     "stun:stun1.l.google.com:19302",
                                     "stun:stun2.l.google.com:19302",
                                     "stun:stun3.l.google.com:19302",
                                     "stun:stun4.l.google.com:19302"]

struct Config {
    let signalingServerUrl: URL
    let webRTCIceServers: [String]
    
    
    static let `default` = Config(signalingServerUrl: defaultSignalingServerUrl, webRTCIceServers: defaultIceServers)
}
