//
//  DisconnectedUser.swift
//  IVN-Demo
//
//  Created by VRLab on 7/16/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation

struct DisconnectedUser:Codable {
    
    let userid:String
    
    
    init(userid:String) {
        self.userid = userid
    }
    
}
