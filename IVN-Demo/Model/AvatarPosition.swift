//
//  AvatarPosition.swift

//  Created by sanam on 6/9/20.
//

import Foundation

enum RoomType {
    
    case  classroom
    case  office
    
}

struct AvatarPosition:Codable{
    let room:String
    let x:Float
    let y:Float
    let z:Float
    let rotationangle:Float
    let userid:String
    
    init(room:String,userid:String,x:Float,y:Float,z:Float,rotation:Float) {
        
        self.x = x
        self.y = y
        self.z = z
        self.rotationangle = rotation
        self.userid = userid
        self.room = room
        
    }
}
