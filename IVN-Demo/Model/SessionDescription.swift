//
//  SessionDescription.swift



import Foundation
import WebRTC

/// This enum is a swift wrapper over `RTCSdpType` for easy encode and decode

enum SdpType: String, Codable {
    case offer, prAnswer, answer
    
    var rtcSdpType: RTCSdpType {
        switch self {
        case .offer:    return .offer
        case .answer:   return .answer
        case .prAnswer: return .prAnswer
        }
    }
}

/// This struct is a swift wrapper over `RTCSessionDescription` for easy encode and decode

struct SessionDescription: Codable {
    let sdp: String
    let type: SdpType
    let userid:String?
    let clientId:String?
    
    init(from rtcSessionDescription: RTCSessionDescription,userid:String,clientid:String) {
        self.sdp = rtcSessionDescription.sdp
        self.userid = userid
        self.clientId = clientid
        switch rtcSessionDescription.type {
        case .offer:    self.type = .offer
        case .prAnswer: self.type = .prAnswer
        case .answer:   self.type = .answer
        @unknown default:
            fatalError("Unknown RTCSessionDescription type: \(rtcSessionDescription.type.rawValue)")
        }
    }
    
    var rtcSessionDescription: RTCSessionDescription {
        return RTCSessionDescription(type: self.type.rtcSdpType, sdp: self.sdp)
    }
}
