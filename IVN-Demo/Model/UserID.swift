//
//  UserID.swift
//  IVN-Demo
//
//  Created by sanam on 7/11/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation

struct UserID:Codable {
    
    let userid:String
    let roomName:String
    let connectedUsers:[String]
    let connectedUserRooms:[String:String]
}
