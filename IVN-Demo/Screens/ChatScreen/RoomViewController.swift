//
//  RoomViewController.swift
//  IVN-Demo
//
//  Created by sanam on 7/19/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import UIKit
import AVFoundation
import SceneKit

class RoomViewController: UIViewController {
    
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var sceneView: SCNView!
    @IBOutlet weak var onlineStatus: UILabel!
    @IBOutlet weak var userConnected: UILabel!
    var datachannelStatus = false
    var signalClient: SignalingClient!
    var webRTCClient: WebRTCClient!
    var userconnected = false
    var mypeer:[String] = [String]()
    var useridRoom = [String]()
    var clientIDToSendAns = ""
    var connecteduserlist = 0
    weak var peerconnnectionHandler:PeerConnectionHandler?
    var videoService:VDService?
    var dataptr:CFData?
    var environment:Environment?
    let pointcloudData = PointCloud()
    var ClinetType:SdpType?
    var roomName:String!
    
    lazy var preview:AVCaptureVideoPreviewLayer = {
        
        let pre = AVCaptureVideoPreviewLayer()
        pre.videoGravity = .resizeAspectFill
        pre.frame = CGRect(x: self.view.bounds.width - 110, y: 60, width: 100, height: 130)
        pre.cornerRadius = 5
        return pre
        
    }()
    
    var signalingConnected: Bool = false {
        didSet {
            DispatchQueue.main.async {
                if self.signalingConnected {
                    self.onlineStatus?.text = "✅"
                    self.onlineStatus?.textColor = UIColor.green
                }
                else {
                    self.onlineStatus?.text = "❌"
                    self.onlineStatus?.textColor = UIColor.red
                }
            }
        }
    }
    
    private var speakerOn: Bool = false {
        didSet {
            let title = "Speaker: \(self.speakerOn ? "On" : "Off" )"
            self.speakerButton?.setTitle(title, for: .normal)
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        peerconnnectionHandler = PeerConnectionHandler.instance
//        self.signalClient = buildSignalingClient()
        self.webRTCClient = WebRTCClient()
//
        self.environment = Environment(sncframe: self.view.bounds, room: self.roomName)
        self.environment?.envDelegate = self
        self.sceneView.addSubview(environment!)


        self.videoService = VDService()

        if !videoService!.hasTrueDepthCamera{
            DepthCamAlert()
        }else{
            print("connection init")
            preview.session = videoService
            self.view.layer.addSublayer(preview)
            videoService!.delegate = self

        }
        setupConnection()
        
        
    }
    
    deinit {
        
        print("controlled deinit")
    }
    
    
    func DepthCamAlert(){
        
        let alert = UIAlertController(title: "Alert", message: "Depth Camera not available", preferredStyle: .alert)
        
        let dismiss = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        
        alert.addAction(dismiss)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
//    func Signalling_webrtc(signalClient: SignalingClient, webRTCClient: WebRTCClient) {
//        self.signalClient = signalClient
//        self.webRTCClient = webRTCClient
//        
//    }
    
    
    
    @IBAction func SpeakerButton(_ sender: UIButton) {
        if self.speakerOn {
            self.webRTCClient.speakerOff()
        }
        else {
            self.webRTCClient.speakerOn()
        }
        self.speakerOn = !self.speakerOn
        
    }
    
    
    func setupConnection(){
       
        print("connection setup")
        self.signalingConnected = false
        self.userconnected = false
        self.speakerOn = false
        self.userConnected?.text = "Not Connected"
        peerconnnectionHandler!.AddConfiguration(iceServers: Config.default.webRTCIceServers)
        self.webRTCClient.delegate = self
        self.signalClient.delegate = self
        self.signalClient.connect()
        
    }
    
//    private func buildSignalingClient() -> SignalingClient {
//
//        // iOS 13 has native websocket support. For iOS 12 or lower we will use 3rd party library.
//        let webSocketProvider: WebSocketProvider
//
//        if #available(iOS 13.0, *) {
//            webSocketProvider = NativeWebSocket(url: Config.default.signalingServerUrl)
//        } else {
//            webSocketProvider = StarscreamWebSocket(url:  Config.default.signalingServerUrl)
//        }
//
//        return SignalingClient(webSocket: webSocketProvider)
//    }
//
    
    func JoinRoom(clientid:String){
        
        
        if self.presentedViewController as? UIAlertController != nil {
            
            
            return
        }
        print ("alert already presented")
        let alert = UIAlertController(title: "Room",
                                      message: "Calling...",
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
            self.userconnected = false
            self.clientIDToSendAns = ""
            self.ClinetType = .none
        }))
        
        alert.addAction(UIAlertAction(title: "Join", style: .default, handler: { [weak self] _ in
            
            self?.webRTCClient.answer(peerconnection:clientid) { (localSdp) in
                self?.clientIDToSendAns = clientid
                print("local sdp ",clientid)
                
                self!.signalClient.send(sdp: localSdp,userid: myuserid,clientid:clientid)
                
            }
            
        }))
        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    func SendOffertoClients(clientId:String){
        
        DispatchQueue.main.async {
            
            print("sending offer to ",clientId)
            self.webRTCClient.offer(peerconnection:clientId) { (sdp) in
                    self.signalClient.send(sdp: sdp,userid:myuserid,clientid: clientId)
                    self.webRTCClient.unmuteAudio(peerconnection:clientId)
                    
                    
                }
        }
    }
    
    
    func OfferCompleted(){
       
        self.connecteduserlist += 1
        
        if self.connecteduserlist < useridRoom.count{
             print("offer complete ",useridRoom)
            self.SendOffertoClients(clientId: useridRoom[connecteduserlist])
        }
        
    }
    
    @IBAction func BackButton(_ sender: Any) {
    
        self.signalClient.disconnect()
//        self.dismiss(animated: false, completion: nil)
    }
}


extension RoomViewController:PointCoudDataDelegate,EnvironmentDelegate{
    
    
    
    
    func sendPointCloudData(data: Data) {
        if datachannelStatus{
            let compressedData = try! (data as NSData).compressed(using: .lzfse)
            self.webRTCClient.sendData(Data(referencing: compressedData))
        }
        
    }
    
    
    
    func decodePoindData(data:Data?,clientId:String){
        
        guard let data = data else {return}
        
        data.withUnsafeBytes { (ptr: UnsafeRawBufferPointer) in
            if let ptrAddress = ptr.baseAddress, ptr.count > 0 {
                let pointer = ptrAddress.assumingMemoryBound(to: UInt8.self) // here you got UnsafePointer<UInt8>
                self.dataptr  = CFDataCreate(kCFAllocatorDefault, pointer, data.count)
                
            } else {
                
                print("error occured")
            }
        }
        let source = CGImageSourceCreateWithData(self.dataptr!, nil)
        
        
        self.environment?.addAvatargeometry(geometry:self.pointcloudData.drawPointCloud(image: UIImage(data: data), depthData: source?.getDisparityData()), clientId: clientId)
        
    }
    
    func didMove(pos: SCNVector3, angle: SCNVector3) {
        if !useridRoom.isEmpty{
               self.signalClient.send(room:"",avatar: pos ,angle: angle.y)
        }
     
    }
}
