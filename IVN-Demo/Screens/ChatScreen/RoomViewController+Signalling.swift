//
//  RoomViewController+Signalling.swift
//  IVN-Demo
//
//  Created by sanam on 7/19/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation
import WebRTC
import SceneKit

extension RoomViewController:SignalClientDelegate{
    
    func signalClientDidConnect(_ signalClient: SignalingClient) {
        self.signalingConnected = true
        self.signalClient.sendUserID(userid: myuserid,room:self.roomName)
    }
    
    func signalClientDidDisconnect(_ signalClient: SignalingClient) {
        DispatchQueue.main.async {
            self.dismiss(animated: false, completion: nil)
                   self.signalingConnected = false
        }
       
    }
    
    func signalClient(_ signalClient: SignalingClient, didReceiveRemoteSdp sdp: RTCSessionDescription, client id: String, client type: SdpType) {
        
        print("received remote sdp")
        self.webRTCClient.set(peerconnection:id ,remoteSdp: sdp) { (error) in
            //            self.hasRemoteSdp = true
            DispatchQueue.main.async {
                if type == .offer{
                    self.ClinetType = type
                    print("client id",id)
                    self.JoinRoom(clientid: id)
                }
                
            }
        }
    }
    
    func signalClient(_ signalClient: SignalingClient, didReceiveCandidate candidate: RTCIceCandidate, client id: String) {
        print("received remote canditate", id)
        self.webRTCClient.set(peerconnection:id,remoteCandidate: candidate)
    }
    
    func signalClient(_ signalClient: SignalingClient, didReceiveAvatarData avatar: AvatarPosition) {
        
        //        print("receiving position")
        self.environment?.moveAvatar(for: avatar.userid, to: SCNVector3(x: avatar.x, y: avatar.y, z: avatar.z),to: avatar.rotationangle)
    }
    
    func signalClient(_ signalClient: SignalingClient, didReceiveUserID userid: UserID) {
        
        self.mypeer = (userid.connectedUsers.map{$0.copy() as! String}).filter({$0 != myuserid})
        self.useridRoom = [String](userid.connectedUserRooms.filter({$0.value == self.roomName}).keys).filter{$0 != myuserid}
        print("my peer array ",self.mypeer," room ",self.useridRoom)
        
        for client in self.useridRoom{
            
            peerconnnectionHandler!.createPeerConnection(client: client)
            self.webRTCClient.addPeerConnection(userpeerconnection: client)
            
            print("pere connection ", peerconnnectionHandler!.getPeerConnection())
            environment?.addAvatar(clinetId: client)
            
            
        }
        
        
        if self.useridRoom.count >= 1{
            
            if userid.connectedUsers.last == myuserid{
                
                SendOffertoClients(clientId: self.useridRoom.first!)
            }
        }else{
            //            DispatchQueue.main.async {
            //                self.showRoomAlert()
            //                print("not in the same room")
            //            }
            
        }
    }
    
    func signalClient(_ signalClient: SignalingClient, didUserDisconnected user: DisconnectedUser) {
        
        print("users disconnected ")
        
        if self.useridRoom.contains(user.userid){
            
            self.webRTCClient.stoppeerconnection(peerconnection: user.userid)
            environment!.removeAvatar(userid: user.userid)
            if let index = mypeer.firstIndex(of: user.userid){
                connecteduserlist -= 1
                mypeer.remove(at: index)
                self.useridRoom = self.useridRoom.filter({$0 != user.userid})
                DispatchQueue.main.async {
                    if self.useridRoom.count == 0{
                        self.userConnected?.text = "Not Connected"
                        self.userConnected?.textColor = .red
                    }else{
                        self.userConnected?.text = "Connected"
                        self.userConnected?.textColor = .green
                    }
                    
                    print("user disconnected ",self.mypeer, "  ",self.useridRoom)
                }
                
            }
            self.peerconnnectionHandler!.removeUser(userid: user.userid)
        }
        
        
    }
    
    
    func showRoomAlert(){
        
        let alert = UIAlertController(title: "Alert", message: "No user online in this Room", preferredStyle: .alert)
        let cancel = UIAlertAction(title: "Dismiss", style: .cancel, handler: nil)
        
        alert.addAction(cancel)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}
