//
//  RoomViewController+Webrtc.swift
//  IVN-Demo
//
//  Created by sanam on 7/19/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import Foundation
import WebRTC

extension RoomViewController:WebRTCClientDelegate{
   
    
    func webRTCClient(_ client: WebRTCClient, didDiscoverLocalCandidate candidate: RTCIceCandidate) {
        
        print("local candidate ")
        if ClinetType == .offer{
            self.signalClient.send(candidate: candidate,userid:myuserid,clientID:self.clientIDToSendAns)
        }else{
            
            self.signalClient.send(candidate: candidate,userid:myuserid,clientID:mypeer[connecteduserlist])
        }
        
    }
    
    func webRTCClient(_ client: WebRTCClient, didChangeConnectionState state: RTCIceConnectionState) {
        let textColor: UIColor
              switch state {
              case .connected, .completed:
                  textColor = .green
                  userconnected = true
              case .disconnected:
                  textColor = .orange
                  userconnected = false
              case .failed, .closed:
                  textColor = .red
              case .new, .checking, .count:
                  textColor = .black
              @unknown default:
                  textColor = .black
              }
              DispatchQueue.main.async {
                  self.userConnected?.text = state.description.capitalized
                  self.userConnected?.textColor = textColor
              }
    }
    
    func webRTCClient(_ client: WebRTCClient, didReceiveData data: Data,clientId:String) {
        
      
        let uncompressedData = try! (data as NSData).decompressed(using: .lzfse)

        let uncomdata = Data(referencing: uncompressedData)
        
        decodePoindData(data: uncomdata,clientId: clientId)
    }
    
    func webRTCClient(_ client: WebRTCClient, datachannelready: Bool) {
        self.datachannelStatus = datachannelready                              
          print("data channel started ", datachannelready)
    }
    
    func webRTCClient(_ client: WebRTCClient, didCompleteIceGathering: Bool) {
        
             if didCompleteIceGathering
             {
                    
                self.OfferCompleted()
                
             }
    }
}
