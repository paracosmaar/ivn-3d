//
//  RoomSelectionViewController.swift
//  IVN-Demo
//
//  Created by sanam on 9/1/20.
//  Copyright © 2020 paracosma. All rights reserved.
//

import UIKit

class RoomSelectionViewController: UIViewController {
    
    var signalingClient:SignalingClient?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.signalingClient = buildSignalingClient()
        
    }
    
    
    func presentViewController(room:String){
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "RoomViewController") as! RoomViewController
        vc.modalPresentationStyle = .fullScreen
        vc.signalClient = signalingClient
        vc.roomName = room
        self.present(vc, animated: true, completion: nil)
        
        
    }
    
    @IBAction func classroomButton(_ sender: Any) {
      
        presentRoom(room: "classroom")
        
    }
    
    @IBAction func officeButton(_ sender: Any) {
     
        presentRoom(room: "office")
    }
    
    func presentRoom(room:String){
        
        if Reachability.isConnectedToNetwork(){
                 presentViewController(room: room)
               }else{
                   
                   Reachability.showInternetAlert(controller: self)
               }
    }
    
    private func buildSignalingClient() -> SignalingClient {
          
          // iOS 13 has native websocket support. For iOS 12 or lower we will use 3rd party library.
          let webSocketProvider: WebSocketProvider
          
          if #available(iOS 13.0, *) {
              webSocketProvider = NativeWebSocket(url: Config.default.signalingServerUrl)
          } else {
              webSocketProvider = StarscreamWebSocket(url:  Config.default.signalingServerUrl)
          }
          
          return SignalingClient(webSocket: webSocketProvider)
      }
      

}
